package com.example.tenantservice.service;

import com.example.tenantservice.controller.dto.Tenant;
import com.example.tenantservice.jpa.TenantEntity;
import com.example.tenantservice.jpa.TenantRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class TenantService{

    private final TenantRepository tenantRepository;

    public Tenant save(Tenant newTenant){
        newTenant.setTenantId(UUID.randomUUID().toString());

        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        TenantEntity saveTenant = tenantRepository.save(mapper.map(newTenant, TenantEntity.class));
        return mapper.map(saveTenant, Tenant.class);
    }

    public List<Tenant> findByAll() {
        Iterable<TenantEntity> tenants = tenantRepository.findAll();

        List<Tenant> result = new ArrayList<>();
        tenants.forEach(v->{
            result.add(new ModelMapper().map(v, Tenant.class));
        });

        return result;
    }

    public Tenant findByTenantId(String tenantId) {
        return new ModelMapper().map(tenantRepository.findByTenantId(tenantId), Tenant.class);
    }

}
