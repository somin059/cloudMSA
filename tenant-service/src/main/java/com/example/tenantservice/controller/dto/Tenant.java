package com.example.tenantservice.controller.dto;

import lombok.Data;

import java.util.Date;

@Data
public class Tenant {
    private String tenantId;
    private String tenantName;
    private Date createTime;
}
