package com.example.tenantservice.controller;

import com.example.tenantservice.controller.dto.Tenant;
import com.example.tenantservice.service.TenantService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/tenant/tenants")
public class TenantController {
    private final TenantService tenantService;

    @GetMapping
    public ResponseEntity<List<Tenant>> Tenents(){
        log.info("[tenant] GET Tenants");
        return ResponseEntity.status(HttpStatus.OK).body(tenantService.findByAll());
    }

    @PostMapping
    public ResponseEntity<Tenant> save(@RequestBody Tenant tenant){
        log.info("[tenant] POST save");
        return ResponseEntity.status(HttpStatus.CREATED).body(tenantService.save(tenant));
    }

    @GetMapping("/{tenantId}")
    public ResponseEntity<Tenant> findByTenantId(@PathVariable("tenantId") String tenantId){
        log.info("[tenant] GET findByTenantId");
        return ResponseEntity.status(HttpStatus.OK).body(tenantService.findByTenantId(tenantId));
    }
}
