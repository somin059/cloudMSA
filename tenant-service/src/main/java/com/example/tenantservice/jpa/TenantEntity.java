package com.example.tenantservice.jpa;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="tenants")
@IdClass(TenantEntityKey.class)
public class TenantEntity {

    @Id
    @Column(nullable = false, unique = true)
    private String tenantId;

    @Id
    @Column(nullable = false)
    private String tenantName;

    @Column(nullable = false, updatable = false, insertable = false)
    @ColumnDefault(value = "CURRENT_TIMESTAMP")
    private Date createTime;
}
