package com.example.tenantservice.jpa;

import org.springframework.data.repository.CrudRepository;

public interface TenantRepository extends CrudRepository<TenantEntity, Long> {
    TenantEntity findByTenantId(String tenantId);
}
