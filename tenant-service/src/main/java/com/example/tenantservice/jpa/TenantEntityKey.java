package com.example.tenantservice.jpa;

import java.io.Serializable;

public class TenantEntityKey implements Serializable {
    private String tenantId;
    private String tenantName;
}
