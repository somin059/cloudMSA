package com.example.agentservice.service;

import com.example.agentservice.jpa.AgentEntity;
import com.example.agentservice.jpa.AgentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AgentService {

    private final AgentRepository agentRepository;

    public void save(AgentEntity agentEntity){
        agentRepository.save(agentEntity);
    }

    public Iterable<AgentEntity> findAll(){
        return agentRepository.findAll();
    }

    public AgentEntity findByTenantIdAndAgentName(String tenantId, String agentName){
        return agentRepository.findByTenantIdAndAgentName(tenantId, agentName);
    }
}
