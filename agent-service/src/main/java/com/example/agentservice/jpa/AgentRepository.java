package com.example.agentservice.jpa;

import org.springframework.data.repository.CrudRepository;

public interface AgentRepository extends CrudRepository<AgentEntity, Long> {
    AgentEntity findByTenantIdAndAgentName(String tenantId, String agentName);
}
