package com.example.agentservice.jpa;

import java.io.Serializable;

public class AgentEntityKey implements Serializable {
    private String tenantId;
    private String agentName;
}
