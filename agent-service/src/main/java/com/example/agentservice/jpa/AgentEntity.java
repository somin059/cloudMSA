package com.example.agentservice.jpa;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import java.util.Date;

@Data
@javax.persistence.Entity
@Table(name="agents")
@IdClass(AgentEntityKey.class)
public class AgentEntity {

    @Id
    @Column(nullable = false)
    private String tenantId;

    @Id
    @Column(nullable = false)
    private String agentName;
    @Column(nullable = false)
    private String requestUrl;
    @Column(nullable = false)
    private String logoutUrl;

    @Column(nullable = false, updatable = false, insertable = false)
    @ColumnDefault(value = "CURRENT_TIMESTAMP")
    private Date createTime;
}
