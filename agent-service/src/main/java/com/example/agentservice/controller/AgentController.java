package com.example.agentservice.controller;

import com.example.agentservice.jpa.AgentEntity;
import com.example.agentservice.service.AgentService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/agent/agents")
public class AgentController {
    private final AgentService agentService;

    @PostMapping
    public ResponseEntity save(@RequestBody AgentEntity agentEntity, RedirectAttributes redirectAttributes){

        agentService.save(agentEntity);

        redirectAttributes.addAttribute("tenantId", agentEntity.getTenantId());
        redirectAttributes.addAttribute("agentName", agentEntity.getAgentName());
        return ResponseEntity.status(HttpStatus.CREATED).body(agentEntity);
    }

    @GetMapping
    public ResponseEntity<List<AgentEntity>> entities(){

        Iterable<AgentEntity> agentEntities = agentService.findAll();

        List<AgentEntity> result = new ArrayList<>();
        agentEntities.forEach(v->{
            result.add(new ModelMapper().map(v, AgentEntity.class));
        });

        return ResponseEntity.status(HttpStatus.OK).body(result);
    }

    @GetMapping("/{tenantId}/{agentName}")
    public ResponseEntity agentEntity (@PathVariable String tenantId,
                              @PathVariable String agentName){
        AgentEntity agentEntity = agentService.findByTenantIdAndAgentName(tenantId, agentName);
        return ResponseEntity.status(HttpStatus.OK).body(agentEntity);
    }
}
