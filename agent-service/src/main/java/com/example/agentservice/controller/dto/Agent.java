package com.example.agentservice.controller.dto;

import lombok.*;

import java.util.Date;

@Data
public class Agent {
    private String tenantId;
    private String agentName;
    private String requestUrl;
    private String logoutUrl;
    private Date createTime;
}
