package com.example.userservice.jpa;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    Iterable<UserEntity> findByTenantId(String tenantId);
    UserEntity findUserByTenantIdAndUserId(String tenantId, String userId);
}
