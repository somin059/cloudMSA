package com.example.userservice.jpa;

import java.io.Serializable;


public class UserEntityKey implements Serializable {
    private String tenantId;
    private String userId;
}
