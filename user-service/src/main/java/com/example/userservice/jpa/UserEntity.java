package com.example.userservice.jpa;

import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="users")
@IdClass(UserEntityKey.class)
public class UserEntity {

    @Id
    @Column(nullable = false)
    private String tenantId;

    @Id
    @Column(nullable = false)
    private String userId;
    @Column(nullable = false)
    private String userName;
    @Column(nullable = false)
    private String type;
    @Column(nullable = false)
    private String status;
    @Column(nullable = false)
    private String pwd;

    @Column(nullable = false, updatable = false, insertable = false)
    @ColumnDefault(value = "CURRENT_TIMESTAMP")
    private Date createTime;

}
