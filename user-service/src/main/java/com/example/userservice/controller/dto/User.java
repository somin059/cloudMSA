package com.example.userservice.controller.dto;

import lombok.Data;

import java.util.Date;

@Data
public class User {
    private String tenantId;
    private String userId;
    private String userName;
    private String status;
    private String type;
    private String pwd;
    private Date createTime;
}
