package com.example.userservice.controller;

import com.example.userservice.controller.dto.User;
import com.example.userservice.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<User>> Users(){
        log.info("[user] GET Users");
        return ResponseEntity.status(HttpStatus.OK).body(userService.findByAll());
    }

    @PostMapping("/users")
    public ResponseEntity<User> save(@RequestBody User user){
        log.info("[user] POST save");
        return ResponseEntity.status(HttpStatus.CREATED).body(userService.save(user));
    }

    @GetMapping("/{tenantId}/users")
    public ResponseEntity<List<User>> findByTenantId(@PathVariable("tenantId") String tenantId) {
        log.info("[user] GET findByTenantId");
        return ResponseEntity.status(HttpStatus.OK).body(userService.findByUserInfo(tenantId));
    }

    @GetMapping(value = "/{tenantId}/{userId}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getUserByTenantIdAndUserId(@PathVariable("tenantId") String tenantId,
                                                           @PathVariable("userId") String userId){
        log.info("[user] GET getUserByTenantIdAndUserId");
        return ResponseEntity.status(HttpStatus.OK).body(userService.findByUserInfo(tenantId, userId));
    }
}
