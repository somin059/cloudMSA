package com.example.userservice.service;

import com.example.userservice.controller.dto.User;
import com.example.userservice.jpa.UserEntity;
import com.example.userservice.jpa.UserRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public List<User> findByAll() {
        Iterable<UserEntity> tenants = userRepository.findAll();

        List<User> result = new ArrayList<>();
        tenants.forEach(v->{
            result.add(new ModelMapper().map(v, User.class));
        });

        return result;
    }

    public User save(User newUser){
        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        UserEntity saveUser = userRepository.save(mapper.map(newUser, UserEntity.class));
        return mapper.map(saveUser, User.class);
    }

    public List<User> findByUserInfo(String tenantId){
        Iterable<UserEntity> users = userRepository.findByTenantId(tenantId);

        List<User> userList = new ArrayList<>();
        users.forEach(v -> {
            userList.add(new ModelMapper().map(v, User.class));
        });

        return userList;
    }

    public User findByUserInfo(String tenantId, String userId){

        ModelMapper mapper = new ModelMapper();
        mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
        UserEntity user = userRepository.findUserByTenantIdAndUserId(tenantId, userId);
        return mapper.map(user, User.class);
    }
}
