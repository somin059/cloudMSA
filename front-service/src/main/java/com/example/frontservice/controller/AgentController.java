package com.example.frontservice.controller;

import com.example.frontservice.domain.Agent;
import com.example.frontservice.domain.Tenant;
import com.example.frontservice.service.AgentService;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@RequestMapping("/front/agents")
@RequiredArgsConstructor
public class AgentController {

    private final AgentService agentService;

    @GetMapping
    public String agents(Model model) {
        model.addAttribute("agents", agentService.findAll().getBody());
        return "agent/agents";
    }

    @GetMapping("/new")
    public String newAgent(){
        return "agent/newAgent";
    }

    @PostMapping("/new")
    public String newAgent(Agent agent, RedirectAttributes redirectAttributes){

        ResponseEntity<Agent> response = agentService.save(agent);
        redirectAttributes.addAttribute("tenantId", response.getBody().getTenantId());
        redirectAttributes.addAttribute("agentName", response.getBody().getAgentName());
        redirectAttributes.addAttribute("status", true);

        return "redirect:/front/agents/{tenantId}/{agentName}";
    }

    @GetMapping("/{tenantId}/{agentName}")
    public String agent(@PathVariable String tenantId, @PathVariable String agentName,
                        String status, Model model){

        ResponseEntity<Agent> response = agentService.findByTenantIdAndAgentName(tenantId, agentName);
        model.addAttribute("agent", response.getBody());
        model.addAttribute("status", status);

        return "agent/agent";
    }

}
