package com.example.frontservice.controller;

import com.example.frontservice.domain.Tenant;
import com.example.frontservice.domain.User;
import com.example.frontservice.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/front/users")
@RequiredArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    public String users(Model model) {
        model.addAttribute("users", userService.findByAll().getBody());
        return "user/users";
    }

    @GetMapping("/new")
    public String newUser(Model model){
        return "user/newUser";
    }

    @PostMapping("/new")
    public String newUser(User user, RedirectAttributes redirectAttributes){

        if(user.getUserId().isEmpty() || user.getUserName().isEmpty() || user.getPwd().isEmpty()){
            log.error("[POST] /front/users/new : userinfo is NULL.");
            return "user/newUser";
        }

        redirectAttributes.addAttribute("tenantId", userService.save(user).getBody().getTenantId());
        redirectAttributes.addAttribute("status", true);

        return "redirect:/front/users/{tenantId}";
    }

    @GetMapping("/{tenantId}")
    public String findByTenantId(@PathVariable String tenantId, Model model){
        model.addAttribute("users", userService.findByTenantId(tenantId).getBody());
        model.addAttribute("tenantId", tenantId);
        return "user/users";
    }
}
