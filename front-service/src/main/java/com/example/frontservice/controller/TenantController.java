package com.example.frontservice.controller;

import com.example.frontservice.domain.Tenant;
import com.example.frontservice.service.TenantService;
import com.netflix.discovery.converters.Auto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Slf4j
@Controller
@RequestMapping("/front/tenants")
@RequiredArgsConstructor
public class TenantController {

    private final TenantService tenantService;

    @GetMapping
    public String Tenants(Model model) {
        log.info("[GET] /front/tenants");
        model.addAttribute("tenants", tenantService.findByAll().getBody());
        log.info(model.getAttribute("tenants").toString());
        return "tenant/tenants";
    }

    @GetMapping("/new")
    public String newTenant(){
        log.info("[GET] /front/tenants/new");
        return "tenant/newTenant";
    }

    @PostMapping("/new")
    public String newTenant(Tenant tenant, RedirectAttributes redirectAttributes){
        log.info("[POST] /front/tenants/new");

        if(tenant.getTenantName().isEmpty()){
            log.error("[POST] /front/tenants/new : TenantName is NULL.");
            return "tenant/newTenant";
        }

        redirectAttributes.addAttribute("tenantId", tenantService.save(tenant).getBody().getTenantId());
        redirectAttributes.addAttribute("status", true);

        log.info(redirectAttributes.getAttribute("tenantId").toString());
        return "redirect:/front/tenants/{tenantId}";
    }

    @GetMapping("/{tenantId}")
    public String tenant(@PathVariable String tenantId, Model model) {
        model.addAttribute("tenant", tenantService.findByTenantId(tenantId).getBody());
        return "tenant/tenant";
    }

    @GetMapping("/{tenantId}/edit")
    public String updateTenant(@PathVariable String tenantId, Model model){

        model.addAttribute("tenant", tenantService.findByTenantId(tenantId).getBody());

        return "tenant/editTenant";
    }

    @PostMapping("/{tenantId}/edit")
    public String updateTenant(@PathVariable String tenantId, Tenant tenant){

//        구현해야함
//        tenantService.update();
//        model.addAttribute("tenant", response.getBody());

        return "redirect:/front/tenants/{tenantId}";
    }
}

