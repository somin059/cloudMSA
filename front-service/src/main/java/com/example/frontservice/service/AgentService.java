package com.example.frontservice.service;

import com.example.frontservice.domain.Agent;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AgentService {

    private final RestTemplate restTemplate;

    public ResponseEntity<List<Agent>> findAll() {
        String requestUrl = String.format("http://apigateway:8000/agent/agents");

        return restTemplate.exchange(requestUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<Agent>>() {});
    }

    public ResponseEntity<Agent> save(Agent agent){
        String requestUrl = String.format("http://apigateway:8000/agent/agents");

        HttpEntity<Agent> request = new HttpEntity<>(agent);
        return restTemplate.exchange(requestUrl, HttpMethod.POST, request,
                        new ParameterizedTypeReference<Agent>() {});
    }

    public ResponseEntity<Agent> findByTenantIdAndAgentName(String tenantId, String agentName){
        String requestUrl = String.format("http://apigateway:8000/agent/agents/%s/%s"
                , tenantId, agentName);

        return restTemplate.exchange(requestUrl, HttpMethod.GET, null,
                        new ParameterizedTypeReference<Agent>() {});
    }
}
