package com.example.frontservice.service;

import com.example.frontservice.domain.Tenant;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TenantService {
    private final RestTemplate restTemplate;

    public ResponseEntity<List<Tenant>> findByAll(){
        String requestUrl = String.format("http://apigateway:8000/tenant/tenants");

        return restTemplate.exchange(requestUrl, HttpMethod.GET, null,
                        new ParameterizedTypeReference<List<Tenant>>() {});
    }

    public ResponseEntity<Tenant> save(Tenant tenant){
        String requestUrl = String.format("http://apigateway:8000/tenant/tenants");
        HttpEntity<Tenant> request = new HttpEntity<>(tenant);
        return restTemplate.exchange(requestUrl, HttpMethod.POST, request,
                new ParameterizedTypeReference<Tenant>() {});
    }

    public ResponseEntity<Tenant> findByTenantId(String tenantId){
        String requestUrl = String.format("http://apigateway:8000/tenant/tenants/%s", tenantId);

        return restTemplate.exchange(requestUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<Tenant>() {});
    }

    public void update(String tenantId){
//        String orderUrl = String.format("http://apigateway:8000/tenant-service/tenants/%s", tenantId);
//        ResponseEntity<Tenant> response =
//                restTemplate.exchange(orderUrl, HttpMethod.GET, null,
//                        new ParameterizedTypeReference<Tenant>() {});
    }

}
