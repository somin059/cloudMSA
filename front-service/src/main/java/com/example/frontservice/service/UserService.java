package com.example.frontservice.service;

import com.example.frontservice.domain.User;
import lombok.RequiredArgsConstructor;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {
    private final RestTemplate restTemplate;

    public ResponseEntity<List<User>> findByAll(){
        String requestUrl = String.format("http://apigateway:8000/user/users");

        return restTemplate.exchange(requestUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<User>>() {});
    }

    public ResponseEntity<User> save(User user){
        String requestUrl = String.format("http://apigateway:8000/user/users");
        user.setStatus("C");

        HttpEntity<User> request = new HttpEntity<>(user);
        return restTemplate.exchange(requestUrl, HttpMethod.POST, request,
                new ParameterizedTypeReference<User>() {});
    }

    public ResponseEntity<List<User>> findByTenantId(String tenantId){
        String requestUrl = String.format("http://apigateway:8000/user/%s/users", tenantId);

        return restTemplate.exchange(requestUrl, HttpMethod.GET, null,
                new ParameterizedTypeReference<List<User>>() {});
    }
}
